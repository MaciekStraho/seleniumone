import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.Map;

public class BulMethods {
    static final String POTENTIAL_REVENUE = "123";

    public static String accountMethod(WebDriver driver, WebDriverWait wait) {

        String accountName = UtilsMethods.generateName(UtilsMethods.NAME_ACC);

        //Create Account
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/Account/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='New']"), 0));
        WebElement NewAccount = driver.findElement(By.xpath("//a[@title='New']"));
        NewAccount.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4WQAS']"), 0));
        WebElement SiteButton = driver.findElement(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4WQAS']"));
        SiteButton.click();

        WebElement NextButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"));
        NextButton.click();

        //Textbox form
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Account Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Account Name']/../following-sibling::input")).sendKeys(accountName);

        driver.findElement(By.xpath(".//span[text()='Phone']/../following-sibling::input")).sendKeys(UtilsMethods.generateNumber(9));

        driver.findElement(By.xpath(".//span[text()='National Identifier']/../following-sibling::input")).sendKeys(UtilsMethods.generateNumber(9));

        //Check box
        driver.findElement(By.xpath(".//span[text()='Validated Account']/.././../div/..//img")).click();

        //Picklist form
        WebElement MarketSegment = driver.findElement(By.xpath(".//span[text()='Market Segment']/../following-sibling::div"));
        MarketSegment.click();
        WebElement MS1 = driver.findElement(By.cssSelector("a[title='A: Agriculture, forestry and fishing']"));
        MS1.click();

        WebElement Segmentation = driver.findElement(By.xpath(".//span[text()='Segmentation']/../following-sibling::div/..//a[text()='--None--']"));
        Segmentation.click();
        WebElement S1 = driver.findElement(By.cssSelector("a[title='Large']"));
        S1.click();

        WebElement RangeOfEmployees = driver.findElement(By.xpath(".//span[text()='Range of employees']/../following-sibling::div/..//a[text()='--None--']"));
        RangeOfEmployees.click();
        WebElement RE1 = driver.findElement(By.cssSelector("a[title='01 - Less than 10']"));
        RE1.click();

        //Adres
        WebElement Adres = driver.findElement(By.xpath(".//span[text()='Country']/../following-sibling::div"));
        Adres.click();
        WebElement Country = driver.findElement(By.cssSelector("a[title='Poland']"));
        Country.click();

        driver.findElement(By.xpath(".//span[text()='Street']/../following-sibling::textarea")).sendKeys(UtilsMethods.accountStreet);
        driver.findElement(By.xpath(".//span[text()='City']/../following-sibling::input")).sendKeys(UtilsMethods.accountCity);
        driver.findElement(By.xpath(".//span[text()='Zip/Postal Code']/../following-sibling::input")).sendKeys(UtilsMethods.accountPostalCode);

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();

        return accountName;
    }

    public static String candidateMethod(WebDriver driver, WebDriverWait wait) {

        String candidateName = UtilsMethods.generateName(UtilsMethods.NAME_CON_CAN);

        //Create Candidate
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/Contact/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='New']"), 0));
        WebElement NewAccount = driver.findElement(By.xpath("//a[@title='New']"));
        NewAccount.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("label[class='slds-radio'][for='0121v00000032fOAAQ']"), 0));
        WebElement CandidateButton = driver.findElement(By.cssSelector("label[class='slds-radio'][for='0121v00000032fOAAQ']"));
        CandidateButton.click();

        WebElement NextButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"));
        NextButton.click();

        //Textbox form
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Last Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Last Name']/../following-sibling::input")).sendKeys(candidateName);

        driver.findElement(By.xpath(".//span[text()='Phone']/../following-sibling::input")).sendKeys(UtilsMethods.generateNumber(9));

        driver.findElement(By.xpath(".//span[text()='Email']/../following-sibling::input")).sendKeys(UtilsMethods.generateEmail(6));


        //Adres
        WebElement Adres = driver.findElement(By.xpath(".//span[text()='Country']/../following-sibling::div"));
        Adres.click();
        WebElement Country = driver.findElement(By.cssSelector("a[title='Poland']"));
        Country.click();

        driver.findElement(By.xpath(".//span[text()='Street']/../following-sibling::textarea")).sendKeys(UtilsMethods.accountStreet);
        driver.findElement(By.xpath(".//span[text()='City']/../following-sibling::input")).sendKeys(UtilsMethods.accountCity);
        driver.findElement(By.xpath(".//span[text()='Zip/Postal Code']/../following-sibling::input")).sendKeys(UtilsMethods.accountPostalCode);

        //Checkbox
        driver.findElement(By.xpath(".//label[@class='label inputLabel uiLabel-left form-element__label uiLabel']/..//span[text()='Privacy Notice']")).click();

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();

        return candidateName;
    }

    public static String clientMethod(WebDriver driver, WebDriverWait wait, String accountName) throws InterruptedException {

        String clientName = UtilsMethods.generateName(UtilsMethods.NAME_CON_CLI);

        //Create Client
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/Contact/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='New']"), 0));
        WebElement NewContact = driver.findElement(By.xpath("//a[@title='New']"));
        NewContact.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4eQAC']"), 0));
        WebElement ClientButton = driver.findElement(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4eQAC']"));
        ClientButton.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"), 0));
        WebElement NextButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"));
        NextButton.click();

        //Textbox form
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Last Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Last Name']/../following-sibling::input")).sendKeys(clientName);

        driver.findElement(By.xpath(".//span[text()='Phone']/../following-sibling::input")).sendKeys(UtilsMethods.generateNumber(9));

        driver.findElement(By.xpath(".//span[text()='Email']/../following-sibling::input")).sendKeys(UtilsMethods.generateEmail(5));

        //Add Account to Clinet
        WebElement accName = driver.findElement(By.xpath(".//span[text()='Account Name']/../following-sibling::div/..//input[@title='Search Accounts']"));
        accName.click();
        driver.findElement(By.xpath(".//span[text()='Account Name']/../following-sibling::div/..//input[@title='Search Accounts']")).sendKeys(accountName);
        Thread.sleep(1000);
        accName.sendKeys(Keys.DOWN);
        accName.sendKeys(Keys.RETURN);

        //Picklist form
        WebElement Department = driver.findElement(By.xpath(".//span[text()='Department']/../following-sibling::div"));
        Department.click();
        WebElement D1 = driver.findElement(By.cssSelector("a[title='Management']"));
        D1.click();

        WebElement Function = driver.findElement(By.xpath(".//span[text()='Function']/../following-sibling::div"));
        Function.click();
        WebElement F1 = driver.findElement(By.cssSelector("a[title='CEO']"));
        F1.click();

        //Adres
        WebElement Adres = driver.findElement(By.xpath(".//span[text()='Mailing Country']/../following-sibling::div"));
        Adres.click();
        WebElement Country = driver.findElement(By.cssSelector("a[title='Poland']"));
        Country.click();

        driver.findElement(By.xpath(".//span[text()='Mailing Street']/../following-sibling::textarea")).sendKeys(UtilsMethods.accountStreet);
        driver.findElement(By.xpath(".//span[text()='Mailing City']/../following-sibling::input")).sendKeys(UtilsMethods.accountCity);
        driver.findElement(By.xpath(".//span[text()='Mailing Zip/Postal Code']/../following-sibling::input")).sendKeys(UtilsMethods.accountPostalCode);

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();

        return clientName;

    }

    //JOB SECTION START
    public static String jobMethod(WebDriver driver, WebDriverWait wait, String accountName, String clientName) throws InterruptedException {

        String jobName = UtilsMethods.generateName(UtilsMethods.NAME_JOB);

        //Create Agreement
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/TR1__Job__c/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//ul[@class='branding-actions slds-button-group slds-m-left--xx-small oneActionsRibbon forceActionsContainer']/..//a[@title='New']"), 0));
        WebElement NewJob = driver.findElement(By.xpath(".//ul[@class='branding-actions slds-button-group slds-m-left--xx-small oneActionsRibbon forceActionsContainer']/..//a[@title='New']"));
        NewJob.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("label[class='slds-radio'][for='0120Y000000VgUvQAK']"), 0));
        WebElement PermanentButton = driver.findElement(By.cssSelector("label[class='slds-radio'][for='0120Y000000VgUvQAK']"));
        PermanentButton.click();

        WebElement NextButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"));
        NextButton.click();

        //Textbox
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Job Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Job Name']/../following-sibling::input")).sendKeys(jobName);

        //Add Account to Job
        WebElement accName = driver.findElement(By.xpath(".//span[text()='Account']/../following-sibling::div/..//input[@title='Search Accounts']"));
        accName.click();
        driver.findElement(By.xpath(".//span[text()='Account']/../following-sibling::div/..//input[@title='Search Accounts']")).sendKeys(accountName);
        Thread.sleep(1000);
        accName.sendKeys(Keys.DOWN);
        accName.sendKeys(Keys.RETURN);

        //Add Client to Job
        WebElement contactName = driver.findElement(By.xpath(".//span[text()='Contact']/../following-sibling::div/..//input[@title='Search Contacts']"));
        contactName.click();
        driver.findElement(By.xpath(".//span[text()='Contact']/../following-sibling::div/..//input[@title='Search Contacts']")).sendKeys(clientName);
        Thread.sleep(1000);
        contactName.sendKeys(Keys.DOWN);
        contactName.sendKeys(Keys.RETURN);

        //Data
        driver.findElement(By.xpath(".//span[text()='Estimated Start Date']/../following-sibling::div/..//input[@class=' input']")).sendKeys(UtilsMethods.generateDate());

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();

        return jobName;

    }

    public static void actionsJob(WebDriver driver, WebDriverWait wait, String candidateName) throws InterruptedException {

        //Add candidate
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Candidates']"), 0));
        driver.findElement(By.xpath(".//span[text()='Candidates']")).click();

        Thread.sleep(4000);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//button[@title='QuickAdd']"), 0));
        driver.findElement(By.xpath(".//button[@title='QuickAdd']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//input[@placeholder='Search..']"), 0));
        WebElement contactName = driver.findElement(By.xpath(".//input[@placeholder='Search..']"));
        contactName.click();
        driver.findElement(By.xpath(".//input[@placeholder='Search..']")).sendKeys(candidateName);
        Thread.sleep(1000);
        driver.findElement(By.xpath(".//li[@data-selectedindex='0']")).click();
//        contactName.sendKeys(Keys.DOWN);
//        contactName.sendKeys(Keys.RETURN);

        //Quick Add
        WebElement quickAdd = driver.findElement(By.xpath("//button[text()='Quick Add']"));
        quickAdd.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//label[text()='Applicant']"),0));
        WebElement Save1 = driver.findElement(By.xpath("//button[text()='Save']"));
        Save1.click();

        //Change stages
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//button[@title='Select view']"), 0));
        Thread.sleep(5000);
        driver.findElement(By.xpath(".//button[@title='Select view']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//button[@title='Select view']/../div/..//a/..//span/../lightning-primitive-icon"),0));
        driver.findElements(By.xpath(".//button[@title='Select view']/../div/..//a/..//span/../lightning-primitive-icon")).get(0).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//input[@class='uiInput uiInputCheckbox uiInput--default uiInput--checkbox']"),0));
        driver.findElement(By.xpath(".//input[@class='uiInput uiInputCheckbox uiInput--default uiInput--checkbox']")).click();

        driver.findElement(By.xpath(".//a[@role='tab']/..//span[text()='Offer']")).click();

        //Save
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//button[text()='Save']"),0));
        Thread.sleep(2000);
        WebElement Save2 = driver.findElement(By.xpath("//button[text()='Save']"));
        Save2.click();

    }

    public static void profileOfInterest(WebDriver driver, WebDriverWait wait) throws InterruptedException {

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Job Name']"), 0));
        WebElement Skills = driver.findElement(By.xpath(".//a[text()='Skills']"));
        Skills.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Language']//../..//div/..//input"), 0));
        WebElement Language = driver.findElement(By.xpath(".//span[text()='Language']//../..//div/..//input"));
        Language.click();
        driver.findElement(By.xpath(".//span[text()='Language']//../..//div/..//input")).sendKeys("Hungarian");
        Thread.sleep(1000);
        Language.sendKeys(Keys.RETURN);

        Thread.sleep(3000);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Language']//../..//div/..//button[@class='slds-button slds-button_icon button-grid-align_absolute-center slds-button_icon-brand']/..//lightning-primitive-icon/*"), 0));
        driver.findElement(By.xpath(".//span[text()='Language']//../..//div/..//button[@class='slds-button slds-button_icon button-grid-align_absolute-center slds-button_icon-brand']/..//lightning-primitive-icon/*")).click();


        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//label[text()='Level']"), 0));
        driver.findElement(By.xpath(".//button[@name='cancel']")).click();

        Thread.sleep(3000);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//button[text()='Save as a Job Template']"), 0));
        driver.findElement(By.xpath(".//button[text()='Save as a Job Template']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Number of profiles']"), 0));
        WebElement poiName = driver.findElement(By.xpath(".//span[text()='Name']/../..//input"));
        poiName.click();
        driver.findElement(By.xpath(".//span[text()='Name']/../..//input")).sendKeys(UtilsMethods.generateDate());

        driver.findElements(By.xpath(".//span[text()='Save']")).get(1).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//div[text()='Profile of Interest']"), 0));
        driver.findElements(By.xpath(".//a[@data-label='Related']")).get(1).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[@title='Skills']"), 0));
        System.out.println(" PRZESZLOOOOO !!!!!!");


    }
    //JOB SECTION END

    public static String[] agreementMethod(WebDriver driver, WebDriverWait wait, String accountName) throws InterruptedException {

        String[] webElementsText = new String[2];

        //Create Agreement
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/AGR_Agreement__c/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='New']"), 0));
        WebElement NewAgreement = driver.findElement(By.xpath("//a[@title='New']"));
        NewAgreement.click();

        //Textbox form
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Agreement Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Agreement Name']/../following-sibling::input")).sendKeys(UtilsMethods.generateName(UtilsMethods.NAME_AGREEMENT));

        //Data
        driver.findElement(By.xpath(".//span[text()='Agreement start date']/../following-sibling::div/..//input[@class=' input']")).sendKeys(UtilsMethods.generateDate());
        driver.findElement(By.xpath(".//span[text()='Agreement end date']/../following-sibling::div/..//input[@class=' input']")).sendKeys(UtilsMethods.generateDate());

        //Add Account to Clinet
        WebElement accName = driver.findElement(By.xpath(".//span[text()='Main Account']/../following-sibling::div/..//input[@title='Search Accounts']"));
        accName.click();
        driver.findElement(By.xpath(".//span[text()='Main Account']/../following-sibling::div/..//input[@title='Search Accounts']")).sendKeys(accountName);
        Thread.sleep(1000);
        accName.sendKeys(Keys.DOWN);
        accName.sendKeys(Keys.RETURN);

        //Picklist form
        WebElement AgreementStatus = driver.findElement(By.xpath(".//span[text()='Agreement Status']/../following-sibling::div/..//a[text()='Pipeline']"));
        AgreementStatus.click();
        WebElement AS1 = driver.findElement(By.xpath(".//a[@title='Pipeline']"));
        AS1.getText();
        webElementsText[0]=AS1.getText();
        WebElement AS2 = driver.findElement(By.xpath(".//a[@title='Pipeline']"));
        AS2.click();

        WebElement AreaScope = driver.findElement(By.xpath(".//span[text()='Area Scope']/../following-sibling::div/..//a[text()='--None--']"));
        AreaScope.click();
        Thread.sleep(1000);
        WebElement AS3 = driver.findElement(By.cssSelector("a[title='International']"));
        AS3.click();

        //Brand List
        WebElement Brand1 = driver.findElement(By.xpath(".//div[@class='actionBody']/..//span[text()='Adecco']"));
        Brand1.getText();
        webElementsText[1]=Brand1.getText();
        WebElement Brand2 = driver.findElement(By.xpath(".//div[@class='actionBody']/..//span[text()='Adecco']"));
        Brand2.click();
        driver.findElements(By.xpath(".//button[@title='Move selection to Chosen']")).get(0).click();

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();

        //Agreement Scope
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Related']"), 0));
        driver.findElement(By.xpath(".//span[text()='Related']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//th[@title='Agreement Scope: Agreement scope ID']/../../../tbody/..//a[@data-navigable='true']"), 0));
        Thread.sleep(2000);
        //driver.findElement(By.xpath(".//th[@title='Agreement Scope: Agreement scope ID']/../../../tbody/..//a[@data-navigable='true']")).click();
        WebElement scope = driver.findElement(By.xpath(".//th[@title='Agreement Scope: Agreement scope ID']/../../../tbody/..//a[@data-navigable='true']"));
        Actions actions = new Actions(driver);
        actions.moveToElement(scope).click().build().perform();

        return webElementsText;

    }

    //OPPORTUNITIE SECTION START
    public static String createOpportunitie(WebDriver driver, WebDriverWait wait, String accountName) throws InterruptedException {


        String opportunitieName = UtilsMethods.generateName(UtilsMethods.NAME_OPP);

        //Create Branch
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/Opportunity/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='New']"), 0));
        WebElement NewOpportunitie = driver.findElement(By.xpath("//a[@title='New']"));
        NewOpportunitie.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4yQAC']"), 0));
        WebElement OpportunitieButton = driver.findElement(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4yQAC']"));
        OpportunitieButton.click();

        WebElement NextButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"));
        NextButton.click();

        //Textbox form
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Opportunity Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Opportunity Name']/../following-sibling::input")).sendKeys(opportunitieName);

        driver.findElement(By.xpath(".//span[text()='Potential Revenue']/../following-sibling::input")).sendKeys(POTENTIAL_REVENUE);

        driver.findElement(By.xpath(".//span[text()='Expected GM %']/../following-sibling::input")).sendKeys("1");

        //Pick list
        WebElement Stage = driver.findElement(By.xpath(".//span[text()='Stage']/../following-sibling::div/..//a[text()='--None--']"));
        Stage.click();
        WebElement S1 = driver.findElement(By.cssSelector("a[title='Identify']"));
        S1.click();

        WebElement Type = driver.findElement(By.xpath(".//span[text()='Type']/../following-sibling::div"));
        Type.click();
        WebElement T1 = driver.findElement(By.cssSelector("a[title='New Client']"));
        T1.click();

        WebElement Service = driver.findElement(By.xpath(".//span[text()='Service']/../following-sibling::div/..//a[text()='--None--']"));
        Service.click();
        WebElement Se1 = driver.findElement(By.cssSelector("a[title='Temporary Staffing']"));
        Se1.click();

        //Add Account to Clinet
        WebElement accName = driver.findElement(By.xpath(".//span[text()='Account Name']/../following-sibling::div/..//input[@title='Search Accounts']"));
        accName.click();
        driver.findElement(By.xpath(".//span[text()='Account Name']/../following-sibling::div/..//input[@title='Search Accounts']")).sendKeys(accountName);
        Thread.sleep(1000);
        accName.sendKeys(Keys.DOWN);
        accName.sendKeys(Keys.RETURN);

        //Data
        driver.findElement(By.xpath(".//span[text()='Close Date']/../following-sibling::div/..//input[@class=' input']")).sendKeys(UtilsMethods.generateDate());

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();

        return opportunitieName;

    }


    public static void createBusinessProposal(WebDriver driver, WebDriverWait wait) throws InterruptedException {

        //New Business Proposal
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//button[@data-aura-class='uiButton']/..//span[text()='Mark Stage as Complete']"), 0));
        driver.findElements(By.xpath(".//button[@data-aura-class='uiButton']/..//span[text()='Mark Stage as Complete']")).get(0).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//div[text()='New Business Proposal']"), 0));
        Thread.sleep(2000);
        driver.findElement(By.xpath(".//div[text()='New Business Proposal']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("button[class='slds-button slds-button--neutral uiButton--brand uiButton forceActionButton']"), 0));
        Thread.sleep(2000);
        WebElement SaveButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral uiButton--brand uiButton forceActionButton']"));
        SaveButton.click();

        //String quoteURL = pobrać urla :)
        Thread.sleep(3000);
        String quoteURL = driver.getCurrentUrl();

        //Approval proces
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//div[text()='Submit for Approval']"), 0));
        WebElement SubmitButton = driver.findElement(By.xpath(".//div[text()='Submit for Approval']"));
        SubmitButton.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Comments']/../following-sibling::textarea"), 0));
        driver.findElement(By.xpath(".//span[text()='Comments']/../following-sibling::textarea")).sendKeys("abc");

        WebElement SubmitButton2 = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral modal-button-left actionButton uiButton--default uiButton--brand uiButton']"));
        SubmitButton2.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Choose Next Approver']/../following-sibling::div/..//input[@title='Search Users']"), 0));
        WebElement accName = driver.findElement(By.xpath(".//span[text()='Choose Next Approver']/../following-sibling::div/..//input[@title='Search Users']"));
        accName.click();
        driver.findElement(By.xpath(".//span[text()='Choose Next Approver']/../following-sibling::div/..//input[@title='Search Users']")).sendKeys("Enxoo Selenium");
        Thread.sleep(1000);
        accName.sendKeys(Keys.DOWN);
        accName.sendKeys(Keys.RETURN);

        WebElement SubmitButton3 = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral modal-button-left actionButton uiButton--default uiButton--brand uiButton']"));
        SubmitButton3.click();

        //A proces
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("button[class='bare slds-button slds-button_icon slds-button_icon slds-button_icon-container slds-button_icon-small slds-global-actions__notifications slds-global-actions__item-action uiButton forceHeaderButton unsNotificationsCounter']"), 0));
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("button[class='bare slds-button slds-button_icon slds-button_icon slds-button_icon-container slds-button_icon-small slds-global-actions__notifications slds-global-actions__item-action uiButton forceHeaderButton unsNotificationsCounter']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Enxoo Selenium is requesting approval for quote']"), 0));
        driver.findElements(By.xpath(".//span[text()='Enxoo Selenium is requesting approval for quote']")).get(0).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//a[@title='Approve']/..//div[text()='Approve']"), 0));
        Thread.sleep(2000);
        driver.findElements(By.xpath(".//a[@title='Approve']/..//div[text()='Approve']")).get(1).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Comments']/../following-sibling::textarea"), 0));
        driver.findElement(By.xpath(".//span[text()='Comments']/../following-sibling::textarea")).sendKeys("cba");

        WebElement ApproveButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral modal-button-left actionButton uiButton--default uiButton--brand uiButton']"));
        ApproveButton.click();

        //przejść na quoteURL :)
        Thread.sleep(2000);
        driver.get(quoteURL);
        System.out.println(quoteURL);

        //Create PDF
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//div[text()='Create PDF']"), 0));
        driver.findElement(By.xpath(".//div[text()='Create PDF']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Choose template']"), 0));
        Thread.sleep(1000);
        driver.findElement(By.xpath(".//span[text()='Create PDF']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Save to Quote']"), 0));
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//span[text()='Save to Quote']")).click();

        //Send Email
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//div[text()='Email Quote']"), 0));
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//div[text()='Email Quote']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//label[text()='Select Template']"), 0));
        driver.findElement(By.xpath(".//option[text()='Send Business Proposal']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//label[@for='CC']/..//abbr[@title='required'"), 0));
        driver.findElement(By.xpath(".//label[@for='CC']/..//abbr[@title='required']")).click();

    }
    //OPPORTUNITIE SECTION END

}
