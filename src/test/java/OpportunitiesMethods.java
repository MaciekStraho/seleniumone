import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OpportunitiesMethods {
    static final String POTENTIAL_REVENUE = "123";

    public static String createOpportunitie(WebDriver driver, WebDriverWait wait, String accountName) throws InterruptedException {


        String opportunitieName = UtilsMethods.generateName(UtilsMethods.NAME_OPP);

        //Create Branch
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/Opportunity/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='New']"), 0));
        WebElement NewOpportunitie = driver.findElement(By.xpath("//a[@title='New']"));
        NewOpportunitie.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4yQAC']"), 0));
        WebElement OpportunitieButton = driver.findElement(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4yQAC']"));
        OpportunitieButton.click();

        WebElement NextButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"));
        NextButton.click();

        //Textbox form
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Opportunity Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Opportunity Name']/../following-sibling::input")).sendKeys(opportunitieName);

        driver.findElement(By.xpath(".//span[text()='Potential Revenue']/../following-sibling::input")).sendKeys(POTENTIAL_REVENUE);

        driver.findElement(By.xpath(".//span[text()='Expected GM %']/../following-sibling::input")).sendKeys("1");

        //Pick list
        WebElement Stage = driver.findElement(By.xpath(".//span[text()='Stage']/../following-sibling::div/..//a[text()='--None--']"));
        Stage.click();
        WebElement S1 = driver.findElement(By.cssSelector("a[title='Identify']"));
        S1.click();

        WebElement Type = driver.findElement(By.xpath(".//span[text()='Type']/../following-sibling::div"));
        Type.click();
        WebElement T1 = driver.findElement(By.cssSelector("a[title='New Client']"));
        T1.click();

        WebElement Service = driver.findElement(By.xpath(".//span[text()='Service']/../following-sibling::div/..//a[text()='--None--']"));
        Service.click();
        WebElement Se1 = driver.findElement(By.cssSelector("a[title='Temporary Staffing']"));
        Se1.click();

        //Add Account to Clinet
        WebElement accName = driver.findElement(By.xpath(".//span[text()='Account Name']/../following-sibling::div/..//input[@title='Search Accounts']"));
        accName.click();
        driver.findElement(By.xpath(".//span[text()='Account Name']/../following-sibling::div/..//input[@title='Search Accounts']")).sendKeys(accountName);
        Thread.sleep(1000);
        accName.sendKeys(Keys.DOWN);
        accName.sendKeys(Keys.RETURN);

        //Data
        driver.findElement(By.xpath(".//span[text()='Close Date']/../following-sibling::div/..//input[@class=' input']")).sendKeys(UtilsMethods.generateDate());

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();

        return opportunitieName;

    }


    public static void createBusinessProposal(WebDriver driver, WebDriverWait wait) throws InterruptedException {

        //New Business Proposal
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//button[@data-aura-class='uiButton']/..//span[text()='Mark Stage as Complete']"), 0));
        driver.findElements(By.xpath(".//button[@data-aura-class='uiButton']/..//span[text()='Mark Stage as Complete']")).get(0).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//div[text()='New Business Proposal']"), 0));
        Thread.sleep(2000);
        driver.findElement(By.xpath(".//div[text()='New Business Proposal']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("button[class='slds-button slds-button--neutral uiButton--brand uiButton forceActionButton']"), 0));
        Thread.sleep(2000);
        WebElement SaveButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral uiButton--brand uiButton forceActionButton']"));
        SaveButton.click();

        //String quoteURL = pobrać urla :)
        Thread.sleep(3000);
        String quoteURL = driver.getCurrentUrl();

        //Approval proces
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//div[text()='Submit for Approval']"), 0));
        WebElement SubmitButton = driver.findElement(By.xpath(".//div[text()='Submit for Approval']"));
        SubmitButton.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Comments']/../following-sibling::textarea"), 0));
        driver.findElement(By.xpath(".//span[text()='Comments']/../following-sibling::textarea")).sendKeys("abc");

        WebElement SubmitButton2 = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral modal-button-left actionButton uiButton--default uiButton--brand uiButton']"));
        SubmitButton2.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Choose Next Approver']/../following-sibling::div/..//input[@title='Search Users']"), 0));
        WebElement accName = driver.findElement(By.xpath(".//span[text()='Choose Next Approver']/../following-sibling::div/..//input[@title='Search Users']"));
        accName.click();
        driver.findElement(By.xpath(".//span[text()='Choose Next Approver']/../following-sibling::div/..//input[@title='Search Users']")).sendKeys("Enxoo Selenium");
        Thread.sleep(1000);
        accName.sendKeys(Keys.DOWN);
        accName.sendKeys(Keys.RETURN);

        WebElement SubmitButton3 = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral modal-button-left actionButton uiButton--default uiButton--brand uiButton']"));
        SubmitButton3.click();

        //A proces
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("button[class='bare slds-button slds-button_icon slds-button_icon slds-button_icon-container slds-button_icon-small slds-global-actions__notifications slds-global-actions__item-action uiButton forceHeaderButton unsNotificationsCounter']"), 0));
        Thread.sleep(5000);
        driver.findElement(By.cssSelector("button[class='bare slds-button slds-button_icon slds-button_icon slds-button_icon-container slds-button_icon-small slds-global-actions__notifications slds-global-actions__item-action uiButton forceHeaderButton unsNotificationsCounter']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Enxoo Selenium is requesting approval for quote']"), 0));
        driver.findElements(By.xpath(".//span[text()='Enxoo Selenium is requesting approval for quote']")).get(0).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//a[@title='Approve']/..//div[text()='Approve']"), 0));
        Thread.sleep(2000);
        driver.findElements(By.xpath(".//a[@title='Approve']/..//div[text()='Approve']")).get(1).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Comments']/../following-sibling::textarea"), 0));
        driver.findElement(By.xpath(".//span[text()='Comments']/../following-sibling::textarea")).sendKeys("cba");

        WebElement ApproveButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral modal-button-left actionButton uiButton--default uiButton--brand uiButton']"));
        ApproveButton.click();

        //przejść na quoteURL :)
        Thread.sleep(2000);
        driver.get(quoteURL);
        System.out.println(quoteURL);

        //Create PDF
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//div[text()='Create PDF']"), 0));
        driver.findElement(By.xpath(".//div[text()='Create PDF']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Choose template']"), 0));
        Thread.sleep(1000);
        driver.findElement(By.xpath(".//span[text()='Create PDF']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Save to Quote']"), 0));
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//span[text()='Save to Quote']")).click();

        //Send Email
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//div[text()='Email Quote']"), 0));
        Thread.sleep(3000);
        driver.findElement(By.xpath(".//div[text()='Email Quote']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//label[text()='Select Template']"), 0));
        driver.findElement(By.xpath(".//option[text()='Send Business Proposal']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//label[@for='CC']/..//abbr[@title='required'"), 0));
        driver.findElement(By.xpath(".//label[@for='CC']/..//abbr[@title='required']")).click();








    }

}

