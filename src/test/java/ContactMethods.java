import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ContactMethods {

    public static String createCandidate(WebDriver driver, WebDriverWait wait) {

        String candidateName = UtilsMethods.generateName(UtilsMethods.NAME_CON_CAN);

        //Create Candidate
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/Contact/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='New']"), 0));
        WebElement NewAccount = driver.findElement(By.xpath("//a[@title='New']"));
        NewAccount.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("label[class='slds-radio'][for='0121v00000032fOAAQ']"), 0));
        WebElement CandidateButton = driver.findElement(By.cssSelector("label[class='slds-radio'][for='0121v00000032fOAAQ']"));
        CandidateButton.click();

        WebElement NextButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"));
        NextButton.click();

        //Textbox form
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Last Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Last Name']/../following-sibling::input")).sendKeys(candidateName);

        driver.findElement(By.xpath(".//span[text()='Phone']/../following-sibling::input")).sendKeys(UtilsMethods.generateNumber(9));

        driver.findElement(By.xpath(".//span[text()='Email']/../following-sibling::input")).sendKeys(UtilsMethods.generateEmail(6));


        //Adres
        WebElement Adres = driver.findElement(By.xpath(".//span[text()='Country']/../following-sibling::div"));
        Adres.click();
        WebElement Country = driver.findElement(By.cssSelector("a[title='Poland']"));
        Country.click();

        driver.findElement(By.xpath(".//span[text()='Street']/../following-sibling::textarea")).sendKeys(UtilsMethods.accountStreet);
        driver.findElement(By.xpath(".//span[text()='City']/../following-sibling::input")).sendKeys(UtilsMethods.accountCity);
        driver.findElement(By.xpath(".//span[text()='Zip/Postal Code']/../following-sibling::input")).sendKeys(UtilsMethods.accountPostalCode);

        //Checkbox
        driver.findElement(By.xpath(".//label[@class='label inputLabel uiLabel-left form-element__label uiLabel']/..//span[text()='Privacy Notice']")).click();

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();

        return candidateName;
    }

    public static String createClient(WebDriver driver, WebDriverWait wait, String accountName) throws InterruptedException {

        String clientName = UtilsMethods.generateName(UtilsMethods.NAME_CON_CLI);

        //Create Client
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/Contact/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='New']"), 0));
        WebElement NewContact = driver.findElement(By.xpath("//a[@title='New']"));
        NewContact.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4eQAC']"), 0));
        WebElement ClientButton = driver.findElement(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4eQAC']"));
        ClientButton.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"), 0));
        WebElement NextButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"));
        NextButton.click();

        //Textbox form
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Last Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Last Name']/../following-sibling::input")).sendKeys(clientName);

        driver.findElement(By.xpath(".//span[text()='Phone']/../following-sibling::input")).sendKeys(UtilsMethods.generateNumber(9));

        driver.findElement(By.xpath(".//span[text()='Email']/../following-sibling::input")).sendKeys(UtilsMethods.generateEmail(5));

        //Add Account to Clinet
        WebElement accName = driver.findElement(By.xpath(".//span[text()='Account Name']/../following-sibling::div/..//input[@title='Search Accounts']"));
        accName.click();
        driver.findElement(By.xpath(".//span[text()='Account Name']/../following-sibling::div/..//input[@title='Search Accounts']")).sendKeys(accountName);
        Thread.sleep(1000);
        accName.sendKeys(Keys.DOWN);
        accName.sendKeys(Keys.RETURN);

        //Picklist form
        WebElement Department = driver.findElement(By.xpath(".//span[text()='Department']/../following-sibling::div"));
        Department.click();
        WebElement D1 = driver.findElement(By.cssSelector("a[title='Management']"));
        D1.click();

        WebElement Function = driver.findElement(By.xpath(".//span[text()='Function']/../following-sibling::div"));
        Function.click();
        WebElement F1 = driver.findElement(By.cssSelector("a[title='CEO']"));
        F1.click();

        WebElement SupportFunction = driver.findElement(By.xpath(".//span[text()='Support Function']/../following-sibling::div"));
        SupportFunction.click();
        WebElement SF1 = driver.findElement(By.cssSelector("a[title='Client']"));
        SF1.click();

        //Adres
        WebElement Adres = driver.findElement(By.xpath(".//span[text()='Mailing Country']/../following-sibling::div"));
        Adres.click();
        WebElement Country = driver.findElement(By.cssSelector("a[title='Poland']"));
        Country.click();

        driver.findElement(By.xpath(".//span[text()='Mailing Street']/../following-sibling::textarea")).sendKeys(UtilsMethods.accountStreet);
        driver.findElement(By.xpath(".//span[text()='Mailing City']/../following-sibling::input")).sendKeys(UtilsMethods.accountCity);
        driver.findElement(By.xpath(".//span[text()='Mailing Zip/Postal Code']/../following-sibling::input")).sendKeys(UtilsMethods.accountPostalCode);

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();
        return clientName;

    }

}
