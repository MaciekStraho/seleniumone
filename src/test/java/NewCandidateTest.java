import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewCandidateTest {

    @Test
    public void newCandidate() throws InterruptedException {
        //Setup
        UtilsMethods bm = new UtilsMethods();
        UtilsMethods.WebWrapper wrapper =  bm.MainSetup();

        //Main test
        UtilsMethods.loginToSalesforce(wrapper.driver, UtilsMethods.UAT_URL);
        wrapper.wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));
        String createdCandidate = ContactMethods.createCandidate(wrapper.driver, wrapper.wait);

        //Get actual account name
        Thread.sleep(9000);
        wrapper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//force-record-layout-item[@role='listitem']/..//span[text()='Name']")));
        String actualCandidate = wrapper.driver.findElement(By.xpath(".//force-record-layout-item[@role='listitem']/..//span[text()='Name']/../../div/..//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11']/..//lightning-formatted-name")).getText();

        //Assertions
        Assertions.assertEquals(createdCandidate, actualCandidate);

        //Teardown
//        driver.quit();

    }

}
