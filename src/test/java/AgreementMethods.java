import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class AgreementMethods {

    public static String[] createAgreement(WebDriver driver, WebDriverWait wait, String accountName) throws InterruptedException {

          String[] webElementsText = new String[2];

        //Create Agreement
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/AGR_Agreement__c/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='New']"), 0));
        WebElement NewAgreement = driver.findElement(By.xpath("//a[@title='New']"));
        NewAgreement.click();

        //Textbox form
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Agreement Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Agreement Name']/../following-sibling::input")).sendKeys(UtilsMethods.generateName(UtilsMethods.NAME_AGREEMENT));

        //Data
        driver.findElement(By.xpath(".//span[text()='Agreement start date']/../following-sibling::div/..//input[@class=' input']")).sendKeys(UtilsMethods.generateDate());
        driver.findElement(By.xpath(".//span[text()='Agreement end date']/../following-sibling::div/..//input[@class=' input']")).sendKeys(UtilsMethods.generateDate());

        //Add Account to Clinet
        WebElement accName = driver.findElement(By.xpath(".//span[text()='Main Account']/../following-sibling::div/..//input[@title='Search Accounts']"));
        accName.click();
        driver.findElement(By.xpath(".//span[text()='Main Account']/../following-sibling::div/..//input[@title='Search Accounts']")).sendKeys(accountName);
        Thread.sleep(1000);
        accName.sendKeys(Keys.DOWN);
        accName.sendKeys(Keys.RETURN);

        //Picklist form
        WebElement AgreementStatus = driver.findElement(By.xpath(".//span[text()='Agreement Status']/../following-sibling::div/..//a[text()='Pipeline']"));
        AgreementStatus.click();
        WebElement AS1 = driver.findElement(By.xpath(".//a[@title='Pipeline']"));
        AS1.getText();
        webElementsText[0]=AS1.getText();
        WebElement AS2 = driver.findElement(By.xpath(".//a[@title='Pipeline']"));
        AS2.click();

        WebElement AreaScope = driver.findElement(By.xpath(".//span[text()='Area Scope']/../following-sibling::div/..//a[text()='--None--']"));
        AreaScope.click();
        Thread.sleep(1000);
        WebElement AS3 = driver.findElement(By.cssSelector("a[title='International']"));
        AS3.click();

        //Brand List
        WebElement Brand1 = driver.findElement(By.xpath(".//div[@class='actionBody']/..//span[text()='Adecco']"));
        Brand1.getText();
        webElementsText[1]=Brand1.getText();
        WebElement Brand2 = driver.findElement(By.xpath(".//div[@class='actionBody']/..//span[text()='Adecco']"));
        Brand2.click();
        driver.findElements(By.xpath(".//button[@title='Move selection to Chosen']")).get(0).click();

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();

        //Agreement Scope
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Related']"), 0));
        driver.findElement(By.xpath(".//span[text()='Related']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//th[@title='Agreement Scope: Agreement scope ID']/../../../tbody/..//a[@data-navigable='true']"), 0));
        Thread.sleep(2000);
        //driver.findElement(By.xpath(".//th[@title='Agreement Scope: Agreement scope ID']/../../../tbody/..//a[@data-navigable='true']")).click();
        WebElement scope = driver.findElement(By.xpath(".//th[@title='Agreement Scope: Agreement scope ID']/../../../tbody/..//a[@data-navigable='true']"));
        Actions actions = new Actions(driver);
        actions.moveToElement(scope).click().build().perform();

        return webElementsText;

    }



}