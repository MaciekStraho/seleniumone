import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountsMethods {

    public static String createAccount(WebDriver driver, WebDriverWait wait) {

        String accountName = UtilsMethods.generateName(UtilsMethods.NAME_ACC);

        //Create Account
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/Account/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='New']"), 0));
        WebElement NewAccount = driver.findElement(By.xpath("//a[@title='New']"));
        NewAccount.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4WQAS']"), 0));
        WebElement SiteButton = driver.findElement(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4WQAS']"));
        SiteButton.click();

        WebElement NextButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"));
        NextButton.click();

        //Textbox form
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Account Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Account Name']/../following-sibling::input")).sendKeys(accountName);

        driver.findElement(By.xpath(".//span[text()='Phone']/../following-sibling::input")).sendKeys(UtilsMethods.generateNumber(9));

        driver.findElement(By.xpath(".//span[text()='National Identifier']/../following-sibling::input")).sendKeys(UtilsMethods.generateNumber(9));

        //Check box
        driver.findElement(By.xpath(".//span[text()='Validated Account']/../following-sibling::input")).click();

        //Picklist form
        WebElement MarketSegment = driver.findElement(By.xpath(".//span[text()='Market Segment']/../following-sibling::div"));
        MarketSegment.click();
        WebElement MS1 = driver.findElement(By.cssSelector("a[title='A: Agriculture, forestry and fishing']"));
        MS1.click();

        WebElement Segmentation = driver.findElement(By.xpath(".//span[text()='Segmentation']/../following-sibling::div/..//a[text()='--None--']"));
        Segmentation.click();
        WebElement S1 = driver.findElement(By.cssSelector("a[title='Large']"));
        S1.click();

        WebElement RangeOfEmployees = driver.findElement(By.xpath(".//span[text()='Range of employees']/../following-sibling::div/..//a[text()='--None--']"));
        RangeOfEmployees.click();
        WebElement RE1 = driver.findElement(By.cssSelector("a[title='01 - Less than 10']"));
        RE1.click();

        //Adres
        WebElement Adres = driver.findElement(By.xpath(".//span[text()='Country']/../following-sibling::div"));
        Adres.click();
        WebElement Country = driver.findElement(By.cssSelector("a[title='Poland']"));
        Country.click();

        driver.findElement(By.xpath(".//span[text()='Street']/../following-sibling::textarea")).sendKeys(UtilsMethods.accountStreet);
        driver.findElement(By.xpath(".//span[text()='City']/../following-sibling::input")).sendKeys(UtilsMethods.accountCity);
        driver.findElement(By.xpath(".//span[text()='Zip/Postal Code']/../following-sibling::input")).sendKeys(UtilsMethods.accountPostalCode);

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();

        return accountName;
    }

}
