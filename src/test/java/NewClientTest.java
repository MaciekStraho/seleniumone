import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

    public class NewClientTest {

    @Test
    public void createClient() throws InterruptedException {
        //Setup
        UtilsMethods bm = new UtilsMethods();
        UtilsMethods.WebWrapper wrapper =  bm.MainSetup();

        //Main test
        UtilsMethods.loginToSalesforce(wrapper.driver, UtilsMethods.UAT_URL);
        wrapper.wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));
        String acc1 = AccountsMethods.createAccount(wrapper.driver, wrapper.wait);

//        for(Integer i = 0; i < 40; i++) {
        String createdClient = ContactMethods.createClient(wrapper.driver, wrapper.wait, acc1);
//        }

        //Get actual account name
        Thread.sleep(9000);
        wrapper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//force-record-layout-item[@role='listitem']/..//span[text()='Name']")));
        String actualClient = wrapper.driver.findElement(By.xpath(".//force-record-layout-item[@role='listitem']/..//span[text()='Name']/../../div/..//div[@class='slds-form-element__control']/..//lightning-formatted-name")).getText();

        //Assertions
        Assertions.assertEquals(createdClient, actualClient);


//        //Teardown
//        driver.quit();

    }

}
