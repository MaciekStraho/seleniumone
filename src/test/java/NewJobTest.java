import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.Map;

public class NewJobTest {

    @Test
    public void createJob() throws InterruptedException {
        //Setup
        UtilsMethods bm = new UtilsMethods();
        UtilsMethods.WebWrapper wrapper =  bm.MainSetup();

        //Admin Login
        UtilsMethods.loginToSalesforce(wrapper.driver, UtilsMethods.UAT_URL);
        wrapper.wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));
        String account = AccountsMethods.createAccount(wrapper.driver, wrapper.wait);
        String client = ContactMethods.createClient(wrapper.driver, wrapper.wait, account);
//        String candidate = ContactMethods.createCandidate(driver, wait);

        //Admin Logout
//        Thread.sleep(5000);
//        driver.findElement(By.xpath(".//span[@class='uiImage']/..//img[@title='User']/..")).click();
//        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//a[@class='profile-link-label logout uiOutputURL']"),0));
//        driver.findElement(By.xpath(".//a[@class='profile-link-label logout uiOutputURL']")).click();

        //Creating a Job
        wrapper.wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));
        String job = JobMethods.createJob(wrapper.driver, wrapper.wait, account, client);

        //Get actual Job name
        Thread.sleep(9000);
        wrapper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//div[@role='listitem']/..//span[text()='Job Name']/../../div/..//span[@class='uiOutputText']")));
        String actualJob = wrapper.driver.findElement(By.xpath(".//div[@role='listitem']/..//span[text()='Job Name']/../../div/..//span[@class='uiOutputText']")).getText();

        //Assertions
        Assertions.assertEquals(job, actualJob);

        //Actions on job
//        JobMethods.actionsJob(driver, wait, candidate);

        //Saving Profile of Interest
        JobMethods.profileOfInterest(wrapper.driver, wrapper.wait);

        //Get Skill
        Thread.sleep(9000);
        wrapper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//div[@role='listitem']/..//span[text()='Job Name']/../../div/..//span[@class='uiOutputText']")));
        String skill = "Hungarian/All";
        String actualSkill = wrapper.driver.findElements(By.xpath(".//span[@title='Master Skill Name']/../../../../../../tbody/..//span[@class='slds-truncate uiOutputTextArea']")).get(1).getText();

        //Assertions
        Assertions.assertEquals(skill, actualSkill);

        //Teardown
        wrapper.driver.quit();

    }

}