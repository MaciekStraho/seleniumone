import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewAgreementTest {

    @Test
    public void createAgreementTest() throws InterruptedException {
        //Setup
        UtilsMethods bm = new UtilsMethods();
        UtilsMethods.WebWrapper wrapper =  bm.MainSetup();

        //Main test
        UtilsMethods.loginToSalesforce(wrapper.driver, UtilsMethods.UAT_URL);
        wrapper.wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));

        String acc1 = AccountsMethods.createAccount(wrapper.driver, wrapper.wait);

//        for(Integer i = 0; i < 2; i++) {
         AgreementMethods.createAgreement(wrapper.driver, wrapper.wait, acc1);
//        }

        //Conditions acceptance
        wrapper.wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Brand']"), 0));
        Thread.sleep(2000);
        wrapper.driver.findElements(By.xpath(".//div[@class='slds-form-element__control slds-grid itemBody']/..//span[text()='Adecco']")).get(1).getText();
        wrapper.driver.findElements(By.xpath(".//div[@class='slds-form-element__control slds-grid itemBody']/..//span[text()='Pipeline']")).get(1).getText();

        //Assertion
        String brandName = "Adecco";
        String status = "Pipeline";
        String actualBrandName = wrapper.driver.findElements(By.xpath(".//div[@class='slds-form-element__control slds-grid itemBody']/..//span[text()='Adecco']")).get(1).getText();
        String actualStatus = wrapper.driver.findElements(By.xpath(".//div[@class='slds-form-element__control slds-grid itemBody']/..//span[text()='Pipeline']")).get(1).getText();

        Assertions.assertEquals(brandName, actualBrandName);
        Assertions.assertEquals(status, actualStatus);

        //Teardown
        wrapper.driver.quit();

    }

}
