import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewOpportunitiesTest {

    @Test
    public void createOpportunitie() throws InterruptedException {
        //Setup
        UtilsMethods bm = new UtilsMethods();
        UtilsMethods.WebWrapper wrapper =  bm.MainSetup();

        //Main test
        UtilsMethods.loginToSalesforce(wrapper.driver, UtilsMethods.UAT_URL);
        wrapper.wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));
        String  acc1 = AccountsMethods.createAccount(wrapper.driver, wrapper.wait);
        String createdOpportunities = OpportunitiesMethods.createOpportunitie(wrapper.driver, wrapper.wait, acc1);

        //To development
        OpportunitiesMethods.createBusinessProposal(wrapper.driver, wrapper.wait);

        //Get actual account name
        Thread.sleep(5000);
        wrapper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//div[@role='listitem']/..//span[text()='Opportunity Name']")));
        String actualOpportunities = wrapper.driver.findElement(By.xpath(".//div[@role='listitem']/..//span[text()='Opportunity Name']/../../div/..//span[@class='uiOutputText']")).getText();

        //Assertions
        Assertions.assertEquals(createdOpportunities + "", actualOpportunities);



        //Teardown
//        wrapper.driver.quit();

    }

}
