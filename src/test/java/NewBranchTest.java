import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewBranchTest {

    @Test
    public void createBranch() throws InterruptedException {
        //Setup
        UtilsMethods bm = new UtilsMethods();
        UtilsMethods.WebWrapper wrapper =  bm.MainSetup();

        //Main test
        UtilsMethods.loginToSalesforce(wrapper.driver, UtilsMethods.UAT_URL);
        wrapper.wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));
        String branchName = BranchMethods.createBranch(wrapper.driver, wrapper.wait);

        //Get actual account name
        Thread.sleep(5000);
        wrapper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//div[@class='test-id__field-label-container slds-form-element__label']/..//span[text()='Branch Name']")));
        String actualBranch = wrapper.driver.findElement(By.xpath(".//div[@class='test-id__field-label-container slds-form-element__label']/..//span[text()='Branch Name']/../../div[@class='slds-form-element__control slds-grid itemBody']/..//span[@class='uiOutputText']")).getText();
        System.out.println(branchName);
        System.out.println(actualBranch);

        //Assertions
        Assertions.assertEquals(branchName, actualBranch);


        //Teardown
//        driver.quit();

    }

}
