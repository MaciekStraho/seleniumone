import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class JobMethods {

    public static String createJob(WebDriver driver, WebDriverWait wait, String accountName, String clientName) throws InterruptedException {

        String jobName = UtilsMethods.generateName(UtilsMethods.NAME_JOB);

        //Create Agreement
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/TR1__Job__c/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//ul[@class='branding-actions slds-button-group slds-m-left--xx-small oneActionsRibbon forceActionsContainer']/..//a[@title='New']"), 0));
        WebElement NewJob = driver.findElement(By.xpath(".//ul[@class='branding-actions slds-button-group slds-m-left--xx-small oneActionsRibbon forceActionsContainer']/..//a[@title='New']"));
        NewJob.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("label[class='slds-radio'][for='0120Y000000VgUvQAK']"), 0));
        WebElement PermanentButton = driver.findElement(By.cssSelector("label[class='slds-radio'][for='0120Y000000VgUvQAK']"));
        PermanentButton.click();

        WebElement NextButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"));
        NextButton.click();

        //Textbox
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Job Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Job Name']/../following-sibling::input")).sendKeys(jobName);

        //Add Account to Job
        WebElement accName = driver.findElement(By.xpath(".//span[text()='Account']/../following-sibling::div/..//input[@title='Search Accounts']"));
        accName.click();
        driver.findElement(By.xpath(".//span[text()='Account']/../following-sibling::div/..//input[@title='Search Accounts']")).sendKeys(accountName);
        Thread.sleep(1000);
        accName.sendKeys(Keys.DOWN);
        accName.sendKeys(Keys.RETURN);

        //Add Client to Job
        WebElement contactName = driver.findElement(By.xpath(".//span[text()='Contact']/../following-sibling::div/..//input[@title='Search Contacts']"));
        contactName.click();
        driver.findElement(By.xpath(".//span[text()='Contact']/../following-sibling::div/..//input[@title='Search Contacts']")).sendKeys(clientName);
        Thread.sleep(1000);
        contactName.sendKeys(Keys.DOWN);
        contactName.sendKeys(Keys.RETURN);

        //Data
        driver.findElement(By.xpath(".//span[text()='Estimated Start Date']/../following-sibling::div/..//input[@class=' input']")).sendKeys(UtilsMethods.generateDate());

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();

        return jobName;


    }

    public static void actionsJob(WebDriver driver, WebDriverWait wait, String candidateName) throws InterruptedException {

        //Add candidate
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Candidates']"), 0));
        driver.findElement(By.xpath(".//span[text()='Candidates']")).click();

        Thread.sleep(4000);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//button[@title='QuickAdd']"), 0));
        driver.findElement(By.xpath(".//button[@title='QuickAdd']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//input[@placeholder='Search..']"), 0));
        WebElement contactName = driver.findElement(By.xpath(".//input[@placeholder='Search..']"));
        contactName.click();
        driver.findElement(By.xpath(".//input[@placeholder='Search..']")).sendKeys(candidateName);
        Thread.sleep(1000);
        driver.findElement(By.xpath(".//li[@data-selectedindex='0']")).click();
//        contactName.sendKeys(Keys.DOWN);
//        contactName.sendKeys(Keys.RETURN);

        //Quick Add
        WebElement quickAdd = driver.findElement(By.xpath("//button[text()='Quick Add']"));
        quickAdd.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//label[text()='Applicant']"),0));
        WebElement Save1 = driver.findElement(By.xpath("//button[text()='Save']"));
        Save1.click();

        //Change stages
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//button[@title='Select view']"), 0));
        Thread.sleep(5000);
        driver.findElement(By.xpath(".//button[@title='Select view']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//button[@title='Select view']/../div/..//a/..//span/../lightning-primitive-icon"),0));
        driver.findElements(By.xpath(".//button[@title='Select view']/../div/..//a/..//span/../lightning-primitive-icon")).get(0).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//input[@class='uiInput uiInputCheckbox uiInput--default uiInput--checkbox']"),0));
        driver.findElement(By.xpath(".//input[@class='uiInput uiInputCheckbox uiInput--default uiInput--checkbox']")).click();

        driver.findElement(By.xpath(".//a[@role='tab']/..//span[text()='Offer']")).click();

        //Save
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//button[text()='Save']"),0));
        Thread.sleep(2000);
        WebElement Save2 = driver.findElement(By.xpath("//button[text()='Save']"));
        Save2.click();

    }

    public static void profileOfInterest(WebDriver driver, WebDriverWait wait) throws InterruptedException {

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Job Name']"), 0));
        WebElement Skills = driver.findElement(By.xpath(".//a[text()='Skills']"));
        Skills.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Language']//../..//div/..//input"), 0));
        WebElement Language = driver.findElement(By.xpath(".//span[text()='Language']//../..//div/..//input"));
        Language.click();
        driver.findElement(By.xpath(".//span[text()='Language']//../..//div/..//input")).sendKeys("Hungarian");
        Thread.sleep(1000);
        Language.sendKeys(Keys.RETURN);

        Thread.sleep(5000);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Language']//../..//div/..//button[@class='slds-button slds-button_icon button-grid-align_absolute-center slds-button_icon-brand']/..//lightning-primitive-icon/*"), 0));
        driver.findElement(By.xpath(".//span[text()='Language']//../..//div/..//button[@class='slds-button slds-button_icon button-grid-align_absolute-center slds-button_icon-brand']/..//lightning-primitive-icon/*")).click();


        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//label[text()='Level']"), 0));
        driver.findElement(By.xpath(".//button[@name='cancel']")).click();

        Thread.sleep(3000);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//button[text()='Save as a Job Template']"), 0));
        driver.findElement(By.xpath(".//button[text()='Save as a Job Template']")).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Number of profiles']"), 0));
        WebElement poiName = driver.findElement(By.xpath(".//span[text()='Name']/../..//input"));
        poiName.click();
        driver.findElement(By.xpath(".//span[text()='Name']/../..//input")).sendKeys(UtilsMethods.generateDate());

        driver.findElements(By.xpath(".//span[text()='Save']")).get(1).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//div[text()='Profile of Interest']"), 0));
        driver.findElements(By.xpath(".//a[@data-label='Related']")).get(1).click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[@title='Skills']"), 0));


    }

}