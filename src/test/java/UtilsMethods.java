import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class UtilsMethods {



    static final String NAME_ACC = "ACCOUNT";
    static final String NAME_CON_CLI = "CLIENT";
    static final String NAME_CON_CAN = "CANDIDATE";
    static final String NAME_BRANCH = "BRANCH";
    static final String NAME_OPP = "OPP";
    static final String NAME_AGREEMENT = "AGREEMENT";
    static final String NAME_JOB = "JOB";

    private static final String NUMERIC_STRING = "0123456789";
    private static final String ALPHANUMERIC_STRING = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxyz";

    static String accountStreet = "Reymonta 22";
    static String accountCity = "Kraków";
    static String accountPostalCode = "30-059";


    public static void loginToSalesforce(WebDriver driver, String s){

        //Open login page
        driver.get(s);
        driver.manage().window().maximize();
        //Input username
        driver.findElement(By.cssSelector("input#username")).sendKeys(UtilsMethods.UAT_USERNAME);
        //Input password
        driver.findElement(By.cssSelector("input#password")).sendKeys(UtilsMethods.UAT_PASSWORD);
        //Click 'Login'
        driver.findElement(By.cssSelector("input#Login")).click();

    }

    public static void bulLoginToSalesforce(WebDriver driver, String s){

        //Open login page
        driver.get(s);
        driver.manage().window().maximize();
        //Input username
        driver.findElement(By.cssSelector("input#username")).sendKeys(UtilsMethods.BUL_USERNAME);
        //Input password
        driver.findElement(By.cssSelector("input#password")).sendKeys(UtilsMethods.BUL_PASSWORD);
        //Click 'Login'
        driver.findElement(By.cssSelector("input#Login")).click();
    }

    //Wrapper Setup for driver
    public class WebWrapper {
        WebDriver driver;
        WebDriverWait wait;
    }

    public UtilsMethods.WebWrapper MainSetup() throws InterruptedException {
        String userFirstName = "User";
        String userLastName = "User";

        //Setup
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", prefs);

        UtilsMethods.WebWrapper wrapper = new UtilsMethods.WebWrapper();

        wrapper.driver = new ChromeDriver(options);
        wrapper.wait = new WebDriverWait(wrapper.driver, 30L, 500L);


        return wrapper;
    }
    //Wrapper Setup for driver END

    public static String generateText(Integer numberOfDigits) {

        StringBuilder sb = new StringBuilder(numberOfDigits);
        for (int i = 0; i < numberOfDigits; i++) {
            int index = (int) (ALPHANUMERIC_STRING.length() * Math.random());
            sb.append(ALPHANUMERIC_STRING.charAt(index));
        }
        System.out.println("Generated number::" + sb.toString());
        return sb.toString();

    }

    public static String generateNumber(Integer numberOfDigits) {

        StringBuilder sb = new StringBuilder(numberOfDigits);
        for (int i = 0; i < numberOfDigits; i++) {
            int index = (int) (NUMERIC_STRING.length() * Math.random());
            sb.append(NUMERIC_STRING.charAt(index));
        }
        System.out.println("Generated number::" + sb.toString());
        return sb.toString();

    }

    public static String generateName(String name) {

        String returnValue;

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        returnValue = name + " " + dtf.format(now);

        System.out.println("Generated name::" + returnValue);

        return returnValue;
    }

    public static String generateEmail(Integer numberOfChars) {

        return UtilsMethods.generateText(numberOfChars) + "@selenium.pl";

    }

    public static String generateDate() {

        String returnValue;

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();
        returnValue = dtf.format(now);

        System.out.println(returnValue);
        return returnValue;


    }

}

