import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BranchMethods {

    public static String createBranch(WebDriver driver, WebDriverWait wait) {

        String branchName = UtilsMethods.generateName(UtilsMethods.NAME_BRANCH);

        //Create Branch
        driver.get("https://focore--devv22.lightning.force.com/lightning/o/BRA_Branch__c/list?filterName=Recent");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='New']"), 0));
        WebElement NewBranch = driver.findElement(By.xpath("//a[@title='New']"));
        NewBranch.click();

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4ZQAS']"), 0));
        WebElement BranchButton = driver.findElement(By.cssSelector("label[class='slds-radio'][for='0120Y000000Qs4ZQAS']"));
        BranchButton.click();

        WebElement NextButton = driver.findElement(By.cssSelector("button[class='slds-button slds-button--neutral slds-button slds-button_brand uiButton']"));
        NextButton.click();

        //Textbox form
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Branch Name']/../following-sibling::input"), 0));
        driver.findElement(By.xpath(".//span[text()='Branch Name']/../following-sibling::input")).sendKeys(branchName);

        driver.findElement(By.xpath(".//span[text()='Email']/../following-sibling::input")).sendKeys(UtilsMethods.generateEmail(6));

        //Pick list
        WebElement Country = driver.findElement(By.xpath(".//span[text()='Country']/../following-sibling::div"));
        Country.click();
        WebElement C1 = driver.findElement(By.cssSelector("a[title='Poland']"));
        C1.click();

        //Saving
        WebElement Save = driver.findElement(By.xpath("//button[@title='Save']"));
        Save.click();

        return branchName;

    }

}

