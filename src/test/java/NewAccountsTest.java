import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewAccountsTest {

    @Test
    public void shouldCorrectLoginToSalesforce() throws InterruptedException {
        //Setup
        UtilsMethods bm = new UtilsMethods();
        UtilsMethods.WebWrapper wrapper =  bm.MainSetup();

        //Login as Admin
        UtilsMethods.loginToSalesforce(wrapper.driver, UtilsMethods.UAT_URL);

        wrapper.wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));
        String acc1 = AccountsMethods.createAccount(wrapper.driver, wrapper.wait);
//        String acc2 = AccountsMethods.createAccount(driver, wait);
//        String acc3 = AccountsMethods.createAccount(driver, wait);
//        String acc4 = AccountsMethods.createAccount(driver, wait);
//        String acc5 = AccountsMethods.createAccount(driver, wait);
//        String acc6 = AccountsMethods.createAccount(driver, wait);
//        String acc7 = AccountsMethods.createAccount(driver, wait);

        //Get actual account name
        Thread.sleep(5000);
        wrapper.wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//div[@role='listitem']/..//span[text()='Account Name']")));
        String actualAccount = wrapper.driver.findElement(By.xpath(".//div[@role='listitem']/..//span[text()='Account Name']/../..//div/..//span[@class='uiOutputText']")).getText();

        //Assertions
        Assertions.assertEquals(acc1, actualAccount);

        //Teardown
        wrapper.driver.quit();

    }

}
