import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.HashMap;
import java.util.Map;

public class BulTest {
    WebDriver driver;
    WebDriverWait wait;

    @BeforeEach
    public  void setup(){
        String userFirstName = "User";
        String userLastName = "User";

        //Setup
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", prefs);

        driver = new ChromeDriver(options);
        wait = new WebDriverWait(driver, 30L, 500L);

        //Login as BUL
        UtilsMethods.bulLoginToSalesforce(driver, UtilsMethods.BUL_URL);

    }

    @AfterEach
    public  void  tearDown() throws InterruptedException {
        driver.quit();
    }

    @Test
    public void newAccount() throws InterruptedException {

//        //Login as BUL
//        UtilsMethods.bulLoginToSalesforce(driver, UtilsMethods.BUL_URL);

        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));
        String acc1 = BulMethods.accountMethod(driver, wait);
//        String acc2 = BulMethods.createAccount(wrapper.driver, wrapper.wait);
//        String acc3 = BulMethods.createAccount(wrapper.driver, wrapper.wait);

        //Get actual account name
        Thread.sleep(5000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//div[@role='list']/..//span[text()='Account Name']")));
        String actualAccount = driver.findElement(By.xpath(".//div[@role='list']/..//span[text()='Account Name']/../..//div/..//span/..//lightning-formatted-text")).getText();

        //Assertions
        Assertions.assertEquals(acc1, actualAccount);

    }

    @Test
    public void newCandidate() throws InterruptedException {

//        //Login as BUL
//        UtilsMethods.bulLoginToSalesforce(driver, UtilsMethods.BUL_URL);

        //Main test
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));
        String newCandidate = BulMethods.candidateMethod(driver, wait);

        //Get actual account name
        Thread.sleep(9000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//force-record-layout-item[@role='listitem']/..//span[text()='Name']")));
        String actualCandidate = driver.findElement(By.xpath(".//force-record-layout-item[@role='listitem']/..//span[text()='Name']/../../div/..//span[@class='test-id__field-value slds-form-element__static slds-grow word-break-ie11']/..//lightning-formatted-name")).getText();

        //Assertions
        Assertions.assertEquals(newCandidate, actualCandidate);

    }

    @Test
    public void newClient() throws InterruptedException {


//        //Login as BUL
//        UtilsMethods.bulLoginToSalesforce(driver, UtilsMethods.BUL_URL);

        //Main test
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));
        String acc1 = BulMethods.accountMethod(driver, wait);

//        for(Integer i = 0; i < 40; i++) {
        String createdClient = BulMethods.clientMethod(driver, wait, acc1);
//        }

        //Get actual account name
        Thread.sleep(9000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//force-record-layout-item[@role='listitem']/..//span[text()='Name']")));
        String actualClient = driver.findElement(By.xpath(".//force-record-layout-item[@role='listitem']/..//span[text()='Name']/../../div/..//div[@class='slds-form-element__control']/..//lightning-formatted-name")).getText();

        //Assertions
        Assertions.assertEquals(createdClient, actualClient);

    }

    @Test
    public void newJob() throws InterruptedException {

//        //Login as BUL
//        UtilsMethods.bulLoginToSalesforce(driver, UtilsMethods.BUL_URL);

        //Main test
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));
        String account = BulMethods.accountMethod(driver, wait);
        String client = BulMethods.clientMethod(driver, wait, account);
//        String candidate = BulMethods.newCandidate(wrapper.driver, wrapper.wait);

        //Creating a Job
        String job = BulMethods.jobMethod(driver, wait, account, client);

        //Get actual account name
        Thread.sleep(9000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//div[@role='listitem']/..//span[text()='Job Name']/../../div/..//span[@class='uiOutputText']")));
        String actualJob = driver.findElement(By.xpath(".//div[@role='listitem']/..//span[text()='Job Name']/../../div/..//span[@class='uiOutputText']")).getText();

        //Assertions
        Assertions.assertEquals(job, actualJob);

        //Actions on job
//        JobMethods.actionsJob(wrapper.driver, wrapper.wait, candidate);

        //Saving Profile of Interest
        JobMethods.profileOfInterest(driver, wait);

        //Get Skill
        Thread.sleep(9000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//div[@role='listitem']/..//span[text()='Job Name']/../../div/..//span[@class='uiOutputText']")));
        String skill = "Hungarian/All";
        String actualSkill = driver.findElements(By.xpath(".//span[@title='Master Skill Name']/../../../../../../tbody/..//span[@class='slds-truncate uiOutputTextArea']")).get(1).getText();

        //Assertions
        Assertions.assertEquals(skill, actualSkill);

    }

    @Test
    public void newAgreement() throws InterruptedException {

//        //Login as a BUL
//        UtilsMethods.bulLoginToSalesforce(driver, UtilsMethods.BUL_URL);

        //Main test
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));

        String acc1 = BulMethods.accountMethod(driver, wait);

//        for(Integer i = 0; i < 2; i++) {
        BulMethods.agreementMethod(driver, wait, acc1);
//        }

        //Conditions acceptance
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(".//span[text()='Brand']"), 0));
        Thread.sleep(2000);
        driver.findElements(By.xpath(".//div[@class='slds-form-element__control slds-grid itemBody']/..//span[text()='Adecco']")).get(1).getText();
        driver.findElements(By.xpath(".//div[@class='slds-form-element__control slds-grid itemBody']/..//span[text()='Pipeline']")).get(1).getText();

        //Assertion
        String brandName = "Adecco";
        String status = "Pipeline";
        String actualBrandName = driver.findElements(By.xpath(".//div[@class='slds-form-element__control slds-grid itemBody']/..//span[text()='Adecco']")).get(1).getText();
        String actualStatus = driver.findElements(By.xpath(".//div[@class='slds-form-element__control slds-grid itemBody']/..//span[text()='Pipeline']")).get(1).getText();

        Assertions.assertEquals(brandName, actualBrandName);
        Assertions.assertEquals(status, actualStatus);

    }

    @Test
    public void newOpportunitie() throws InterruptedException {

//        //Login as a BUL
//        UtilsMethods.bulLoginToSalesforce(driver, UtilsMethods.BUL_URL);

        //Main test
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a[@title='Accounts']/..//span[text()='Accounts']"), 0));
        String  acc1 = BulMethods.accountMethod(driver, wait);
        String createdOpportunities = BulMethods.createOpportunitie(driver, wait, acc1);

        //To development
        BulMethods.createBusinessProposal(driver, wait);

        //Get actual account name
        Thread.sleep(5000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//div[@role='listitem']/..//span[text()='Opportunity Name']")));
        String actualOpportunities = driver.findElement(By.xpath(".//div[@role='listitem']/..//span[text()='Opportunity Name']/../../div/..//span[@class='uiOutputText']")).getText();

        //Assertions
        Assertions.assertEquals(createdOpportunities + "", actualOpportunities);

    }

}
